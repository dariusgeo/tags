<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="test" uri="http://viralpatel.net/blogs/jsp/taglib/substr"%>
<html>
<head>
<title>Custom Tags</title>
</head>
<body>
	Before Custom Tag<br>
	<test:substring input="GOODMORNING" start="1" end="6"/><br>
	After Custom Tag<br>
</body>
</html>